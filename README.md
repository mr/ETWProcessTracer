# ETWProcessTracer [PoC]
**Version: 0.1**
 
A simple PoC tool to analyse process behaviour with Event Tracing for Windows (ETW) written in C# and build on the Microsoft.Diagnostics.Tracing.TraceEvent library. 



### Requirements
- Microsoft.NET Framework 4.5
- Admin privileges (required to create ETW sessions)



# Usage 

## Start trace/analysis 
**Default:** Capture events of all registered ETW providers + Kernel providers by KernelKeyword "Process")
```text
ETWProcessTracer --PID [ProcessID]
ETWProcessTracer --PID 1234
```


## Configure providers (User-/Kernel-Mode)
### Select specific KernelKeywords or Keyword-Collections
```text
--KernelKeywords [Keyword(s) | Keyword-Collection]

--KernelKeywords Forensic
--KernelKeywords ImageLoad,NetworkTCPIP,...
```
**Available keywords (TraceEvent library):**
```text
DiskFileIO, DiskIO, ImageLoad, MemoryHardFaults, NetworkTCPIP, ProcessCounters, Profile, Thread, ContextSwitch, DiskIOInit, Dispatcher, FileIO, FileIOInit, Memory, Registry, SystemCall, VirtualAlloc, VAMap, AdvancedLocalProcedureCalls, DeferedProcedureCalls, Driver, Interrupt, SplitIO
```

**Keyword-Collections (TraceEvent library):**
```text
All (Keywords: Verbose, ContextSwitch, Dispatcher, FileIO, FileIOInit, Memory, Registry, VirtualAlloc, VAMap, SystemCall, OS)
Default (Keywords: DiskIO, DiskFileIO, DiskIOInit, ImageLoad, MemoryHardFaults, NetworkTCPIP, Process, ProcessCounters, Profile, Thread)
Verbose (Keywords: Default, ContextSwitch, Dispatcher, FileIO, FileIOInit, Memory, Registry, VirtualAlloc, VAMapThreadTime 
```
**Forensic Keyword-Collection (ETWProcessTracer):**
```text
Forensic (Keywords: Process, Thread, ImageLoad, NetworkTCPIP, FileIOInit, Registry)
```


### Select specific ETW providers (by list)
```text
--ProviderConfig [PATH]
--ProviderConfig C:\etw\providers.txt
```
**List format:** ProviderName {GUID}   
e.g. `providers.txt`:
```text
Microsoft-Windows-DNS-Client             {1C95126E-7EEA-49A9-A3FE-A378B03DDB4D}
Microsoft-Windows-WinINet                {43D1A55C-76D6-4F7E-995C-64C711E5CAFE}
...
```
Tip: Use `logman query providers` and copy & paste interesting providers or use the exampleProviders.txt

## Additional options
### Add additional process(es) by name (e.g. 'powershell')
`--ProcessName` will add new spawned process(es) automatically to the process watchlist for full analysis:
```text
ETWProcessTracer --PID [ProcessID] --ProcessName [ProcessName(s)]
ETWProcessTracer --PID 1234 --ProcessName powershell,msiexec,wmiprvse,...
```


### Auto-detect spawned process(es) by name (e.g. 'WmiPrvSE')
The option `--SpawnDetection` will observe already running and new created instances of the given process name. Spawned child processes will be automatically added to the process watchlist for full analysis.
```text
ETWProcessTracer --PID [ProcessID] --SpawnDetection [ProcessName(s)]
ETWProcessTracer --PID 1234 --SpawnDetection powershell,msiexec,wmiprvse,...
```

#### Combine options `--ProcessName` and `--SpawnDetection`
Both options can be set simultaneously, but keep in mind that option `--ProcessName` has a higher precedence than `--SpawnDetection` (in case a process name is specified in both options).
```text
ETWProcessTracer --PID [ProcessID] --ProcessName [ProcessName(s)] --SpawnDetection [ProcessName(s)]
ETWProcessTracer --PID [ProcessID] --ProcessName powershell,msiexec --SpawnDetection wmiprvse
```

## Stop trace/analysis
`[CTRL+C]` will stop capturing events and terminate the ETW-Session. The captured events will be written into a CSV log file. This could take some time, esp. if many events were beeing captured. A timer can be specified by option `--timer [sec]`.


## Log file (CSV)
The log file (CSV format) will be created in the same directory with filename pattern `"yyyy-MM-dd-HHmm_LOG_PID-[InitialProcessID].csv"`. The default path can be changed by the option `--dir [PATH]`.  

**Log file header line:**  

| ID  | Time | ProviderName  | EventName | EventMessage  | 
|------------- | ------------- |------------- | ------------- | ------------- |

## Useful providers and events (e.g. for malware forensic analysis)
This list of providers and events is not not exhaustive. More providers and events can be identified during further analysis. Upcoming events depends on behaviour.

### Process / Threads
| Provider  | EventName(s) |
|------------- | ------------- |
| Microsoft-Windows-Kernel-Process  | ImageLoad, ImageUnload, ProcessStart/Start, ProcessStart/Stop, ThreadStart/Start  |
| Microsoft-Windows-Kernel-Prefetch  | ScenarioDecision |
| Windows Kernel: Process / process   | Process/DCStart, Process/Start, Process/Stop  |
| Windows Kernel: ImageLoad / img   | Image/DCStart, Image/Load, Image/Unload  |
| Microsoft-Windows-DotNETRuntime   | Exception/Start, Method/InliningFailed  |

### File system
| Provider  | EventName(s) |
|------------- | ------------- |
| Microsoft-Windows-Kernel-File  | Create, CreateNewFile, NameCreate, RenamePath  |
| Microsoft-Windows-FileInfoMiniﬁlter   | ﬁ:FileNameCreate |
| Microsoft-Windows-Shell-Core  | CDesktopFolder_ParseDisplayName/Start, Shell32_CopyEngine_FileOperation_Info, ShellTask_ExecAssoc_ZoneCheckFile/Stop  |
| Windows Kernel: FileIO / ﬁleiocompletion | FileIO/Cleanup, FileIO/Close, FileIO/Create, FileIO/Delete, FileIO/DirEnum, FileIO/FSControl, FileIO/MapFile, FileIO/QueryInfo, FileIO/Read, FileIO/UnMapFile, FileIO/Write        |

### Windows-Registry
| Provider  | EventName(s) |
|------------- | ------------- |
| Microsoft-Windows-Kernel-Registry  | EventID(1): CreateKey, EventID(2): OpenKey, EventID(5): SetValueKey, EventID(7): QueryValueKey  |
| Windows Kernel: Registry  | Registry/KCBCreate, Registry/Open  |

### Network communication
| Provider  | EventName(s) |
|------------- | ------------- |
| Microsoft-Windows-CAPI2  | BuildChain/Stop, RetrieveObjectfromNetwork/Stop, VerifyChainPolicy, VerifyRevocation/Stop, VerifyTrust/Stop, X509Objects  |
| Microsoft-Windows-DNS-Client  | EventID(3006), EventID(3007), EventID(3008)  |
| Microsoft-Windows-MPS-CLNT   | MPS_CLNT_API_SetFirewallRule/Start, MPS_CLNT_API_SetFirewallRule/Stop  |
| Microsoft-Windows-RPC   | RpcClientCall/Start  |
| Microsoft-Windows-TCPIP   | TcpipRouteLookup, TcpConnectTcbProceeding, TcpRequestConnect, TcpConnectRestransmit  |
| Microsoft-Windows-URLMon   | URLMON_CInet_Start, URLMON_Queue_Msg, URLMON_Process_Queued_Msg, URLMON_CINet_Read    |
| Microsoft-Windows-WebIO   | RequestCreate, ConnectionNameResolution/Start, ConnectionNameResolution/Stop  |
| Microsoft-Windows-WinINet   | Wininet_Connect/Stop, WININET_CONNECT_HANDLE_CREATED, WININET_DNS_QUERY/Start, WININET_DNS_QUERY/Stop, Wininet_Getaddrinfo/Start, WININET_HTTP_REQUEST, WININET_HTTP_REQUEST_HANDLE_CREATED_HANDLE_CREATED, WININET_HTTP_RESPONSE_BODY_RECEIVED, WININET_HTTPS_SERVER_CERT_VALIDATED, WININET_REQUEST_HEADER, WININET_REQUEST_HEADER_OPTIONAL, Wininet_ResolveHost/Start, WININET_RESPONSE_HEADER, Wininet_SendRequest/Start, Wininet_SendRequest/Start, WININET_TCP_CONNECTION/Start,  Wininet_UsageLogRequest |
| Microsoft-Windows-WinINet-Capture   | EventID(2001), EventID(2002), EventID(2003), EventID(2004)  |
| Microsoft-Windows-Winsock-AFD   | AfdAbort/Aborted, AfdBindWithAddress/Open, AfdClose/Closed, AfdConnectWithAddress/Bound, AfdReceive/Connected, AfdSend/Connected   |
| Microsoft-Windows-WebIO   | RequestCreate, ConnectionNameResolution/Start, ConnectionNameResolution/Stop  |
| Microsoft-Windows-Winsock-NameResolution   | WinsockGai  |
|  Windows Kernel: TcpIp   | TcpIp/Connect, TcpIp/Disconnect, TcpIp/Recv, TcpIp/Send    |

### Scripting (PowerShell)
| Provider  | EventName(s) |
|------------- | ------------- |
| Microsoft-Windows-PowerShell    | ExecuteaRemoteCommand/Oncreatecalls, StartingCommand/Tobeusedwhenoperationisjustexecutingamethod, StartingEngine/Tobeusedwhenoperationisjustexecutingamethod, StartingProvider/Tobeusedwhenoperationisjustexecutingamethod    |


## Further information
### Event Tracing for Windows (ETW):
- https://docs.microsoft.com/en-us/windows/win32/etw/about-event-tracing
- https://docs.microsoft.com/en-us/archive/msdn-magazine/2007/april/event-tracing-improve-debugging-and-performance-tuning-with-etw

### Other projects and related work that focus on ETW:
- fireeye/SilkETW (https://github.com/fireeye/SilkETW)
- matthastings/PSalander (https://github.com/matthastings/PSalander/tree/master/PSWasp)
- airbus-cert/Winshark (https://github.com/airbus-cert/Winshark)
- airbus-cert/etl-parser https://github.com/airbus-cert/etl-parser
- zacbrown/PowerKrabsEtw (https://github.com/zacbrown/PowerKrabsEtw)
- zodiacon/ProcMonX (https://github.com/zodiacon/ProcMonX)
- zodiacon/EtwExplorer (https://github.com/zodiacon/EtwExplorer)
- lallousx86/WinTools (https://github.com/lallousx86/WinTools/tree/master/WEPExplorer)



##  Used Third-Party libraries
> Microsoft.Diagnostics.Tracing.TraceEvent, Copyright (c) .NET Foundation and Contributors  
> (MIT license see: https://github.com/Microsoft/perfview/blob/master/LICENSE.TXT)

> CommandLineParser, Copyright (c) 2005 - 2015 Giacomo Stelluti Scala & Contributors  
> (MIT license see: https://github.com/commandlineparser/commandline/blob/master/License.md)
