﻿using CommandLine;
using Microsoft.Diagnostics.Tracing;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Diagnostics.Tracing.Session;
using Microsoft.Diagnostics.Tracing.Parsers;
using System.IO;
using System.Threading;
using Microsoft.Diagnostics.Tracing.Parsers.Kernel;
using System.Diagnostics;

namespace ETWProcessTracer {
    class Program {
        private static int eventCounter = 0;
        private static int timersec = 0;
        private static int initPID = 999999;
        private static string providerList = "";
        private static string outFolder = "";
        private static List<string> pname;
        private static List<string> spawndetection;
        private static KernelTraceEventParser.Keywords kernelKeywords;

        static int Main(string[] args) {
            Console.WriteLine("-------------------------------------------------------------------------------------------------");
            Console.ForegroundColor = ConsoleColor.DarkRed;
            Console.WriteLine("                            xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx");
            Console.WriteLine("                                 [PoC] EtwProcessTracer (v.0.1)   ");
            Console.WriteLine("                            xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx");
            Console.ResetColor();
            Console.ForegroundColor = ConsoleColor.DarkGray;
            Console.WriteLine("\nUsed packages: ");
            Console.WriteLine("-Package Microsoft.Diagnostics.Tracing.TraceEvent, Copyright (c) .NET Foundation and Contributors \n MIT license see: https://github.com/Microsoft/perfview/blob/master/LICENSE.TXT");
            Console.WriteLine("-Package CommandLineParser, Copyright (c) 2005 - 2015 Giacomo Stelluti Scala & Contributors \n MIT license see: https://github.com/commandlineparser/commandline/blob/master/License.md ");
            Console.ResetColor();
            Console.WriteLine("-------------------------------------------------------------------------------------------------\n");

            var cmdArgs = Parser.Default.ParseArguments<CommandLineOptions>(args);
            cmdArgs.WithParsed(
                options => {
                    RunOptions(options);
                });

            if (!(TraceEventSession.IsElevated() ?? false)) {
                Console.ForegroundColor = ConsoleColor.Yellow;
                Console.WriteLine("[WARNING] User \"" + Environment.UserDomainName + "\\" + Environment.UserName + "\" is not elevated. Starting ETW-Session requires Admin!");
                Console.ResetColor();
            }

            return 0;
        }

        private static int RunOptions(CommandLineOptions cmdArgs) {
            // Display config summary for review & set vars from args
            Console.WriteLine("CONFIGURATION:");
            Console.WriteLine("____________________________________________________________________________________________\n");
            if (cmdArgs.ArgProcessID != 0) {
                Console.WriteLine($"Target ProcessID: \t{cmdArgs.ArgProcessID}");
                initPID = cmdArgs.ArgProcessID;
            }

            pname = cmdArgs.ArgPNames.ToList();
            Console.Write($"Additional Process(es): \t");
            Console.WriteLine(String.Join(", ", pname));

            spawndetection = cmdArgs.ArgSpawnDetection.ToList();
            Console.Write($"SpawnDetection: \t");
            Console.WriteLine(String.Join(", ", spawndetection));


            if (!String.IsNullOrEmpty(cmdArgs.ArgProviders)) {
                Console.WriteLine($"ProviderConfig: \t{cmdArgs.ArgProviders}");
                providerList = cmdArgs.ArgProviders;
            }
            else
                Console.WriteLine($"ProviderConfig: \tall registered ETW-Providers");

            // Add KernelKeyword Parsers
            kernelKeywords = (KernelTraceEventParser.Keywords.Process);
            foreach (string k in cmdArgs.ArgKernelKeywords.ToList()) {
                switch (k) {
                    case "DiskFileIO":
                        kernelKeywords |= KernelTraceEventParser.Keywords.DiskFileIO;
                        break;
                    case "DiskIO":
                        kernelKeywords |= KernelTraceEventParser.Keywords.DiskIO;
                        break;
                    case "ImageLoad":
                        kernelKeywords |= KernelTraceEventParser.Keywords.ImageLoad;
                        break;
                    case "MemoryHardFaults":
                        kernelKeywords |= KernelTraceEventParser.Keywords.MemoryHardFaults;
                        break;
                    case "NetworkTCPIP":
                        kernelKeywords |= KernelTraceEventParser.Keywords.NetworkTCPIP;
                        break;
                    case "ProcessCounters":
                        kernelKeywords |= KernelTraceEventParser.Keywords.ProcessCounters;
                        break;
                    case "Profile":
                        kernelKeywords |= KernelTraceEventParser.Keywords.Profile;
                        break;
                    case "Thread":
                        kernelKeywords |= KernelTraceEventParser.Keywords.Thread;
                        break;
                    case "ContextSwitch":
                        kernelKeywords |= KernelTraceEventParser.Keywords.ContextSwitch;
                        break;
                    case "DiskIOInit":
                        kernelKeywords |= KernelTraceEventParser.Keywords.DiskIOInit;
                        break;
                    case "Dispatcher":
                        kernelKeywords |= KernelTraceEventParser.Keywords.Dispatcher;
                        break;
                    case "FileIO":
                        kernelKeywords |= KernelTraceEventParser.Keywords.FileIO;
                        break;
                    case "FileIOInit":
                        kernelKeywords |= KernelTraceEventParser.Keywords.FileIOInit;
                        break;
                    case "Memory":
                        kernelKeywords |= KernelTraceEventParser.Keywords.Memory;
                        break;
                    case "Registry":
                        kernelKeywords |= KernelTraceEventParser.Keywords.Registry;
                        break;
                    case "SystemCall":
                        kernelKeywords |= KernelTraceEventParser.Keywords.SystemCall;
                        break;
                    case "VirtualAlloc":
                        kernelKeywords |= KernelTraceEventParser.Keywords.VirtualAlloc;
                        break;
                    case "VAMap":
                        kernelKeywords |= KernelTraceEventParser.Keywords.VAMap;
                        break;
                    case "AdvancedLocalProcedureCalls":
                        kernelKeywords |= KernelTraceEventParser.Keywords.AdvancedLocalProcedureCalls;
                        break;
                    case "DeferedProcedureCalls":
                        kernelKeywords |= KernelTraceEventParser.Keywords.DeferedProcedureCalls;
                        break;
                    case "Driver":
                        kernelKeywords |= KernelTraceEventParser.Keywords.Driver;
                        break;
                    case "Interrupt":
                        kernelKeywords |= KernelTraceEventParser.Keywords.Interrupt;
                        break;
                    case "SplitIO":
                        kernelKeywords |= KernelTraceEventParser.Keywords.SplitIO;
                        break;
                    case "ThreadTime":
                        kernelKeywords |= KernelTraceEventParser.Keywords.ThreadTime;
                        break;
                    case "Forensic":
                        kernelKeywords |= (KernelTraceEventParser.Keywords.Thread | KernelTraceEventParser.Keywords.ImageLoad | KernelTraceEventParser.Keywords.NetworkTCPIP | KernelTraceEventParser.Keywords.FileIOInit | KernelTraceEventParser.Keywords.Registry);
                        break;
                    case "All":
                        kernelKeywords |= KernelTraceEventParser.Keywords.All;
                        break;
                    case "Default":
                        kernelKeywords |= KernelTraceEventParser.Keywords.Default;
                        break;
                    case "Verbose":
                        kernelKeywords |= KernelTraceEventParser.Keywords.Verbose;
                        break;
                    case "OS":
                        kernelKeywords |= KernelTraceEventParser.Keywords.OS;
                        break;
                    default:
                        break;
                }
            }

            Console.WriteLine($"KernelKeywords: \t{kernelKeywords}");

            if (!String.IsNullOrEmpty(cmdArgs.ArgOutFolder)) {
                outFolder = cmdArgs.ArgOutFolder;
                Directory.CreateDirectory(outFolder);
                if (!outFolder.Substring(outFolder.Length - 1).Equals("\\")) {
                    outFolder += "\\";
                }
            }
            else
                outFolder = Directory.GetCurrentDirectory() + "\\";

            Console.WriteLine($"\nLogfile directory: \t{outFolder}");

            if (cmdArgs.ArgTimer != 0) {
                Console.WriteLine($"Timer:  \t\t{cmdArgs.ArgTimer}s");
                timersec = cmdArgs.ArgTimer;
            }
            else
                Console.WriteLine("Timer: \t\t\tnot set");

            Console.WriteLine("____________________________________________________________________________________________\n");

            // Stop info and Start information
            Console.ForegroundColor = ConsoleColor.White;
            Console.WriteLine("\n[INFO] Press CTRL+C at any time to stop tracing");
            Console.WriteLine("Press <Enter> to start monitoring...");
            Console.ResetColor();
            while (Console.ReadKey().Key != ConsoleKey.Enter) { }
            Console.ForegroundColor = ConsoleColor.DarkGreen;
            Console.WriteLine("\nProcess Trace monitoring started");
            Console.ResetColor();

            // Start analysis
            Thread ETWAnalyzer = new Thread(ProcessUserKernelLive);
            ETWAnalyzer.Start();

            return 0;
        }

        private static void ProcessUserKernelLive() {
            // PID Watchlist
            List<int> pidWatchlist = new List<int> { initPID };
            // PID SpawnDetection Watchlist
            List<int> spawnDetectPIDs = new List<int> { };

            using (TraceEventSession session = new TraceEventSession("ETWProcessTracerSession")) {
                // Session options
                session.StopOnDispose = true;

                // CTRL+C Callback
                Console.CancelKeyPress += new ConsoleCancelEventHandler((object sender, ConsoleCancelEventArgs cancelArgs) => {
                    Console.ForegroundColor = ConsoleColor.DarkRed;
                    Console.WriteLine("\n\n[CTRL+C] ETWProcessTracerSession will be terminated. Please wait...");
                    Console.ResetColor();
                    session.Dispose();
                    cancelArgs.Cancel = true;
                });

                // CSV-Output Log-File 
                var csvFile = new StringBuilder();
                var csvFileName = outFolder + DateTime.Now.ToString("yyyy-MM-dd-HHmm") + @"_LOG_PID-" + initPID + ".csv";
                var header = string.Format("{0};{1};{2};{3};{4};{5}", "ID", "Time", "PID", "ProviderName", "EventName", "EventMessage");
                csvFile.AppendLine(header);

                // Enable KernelTrace Providers
                session.EnableKernelProvider(kernelKeywords);

                // Enable UserTrace Providers
                if (!providerList.Equals("")) {
                    // Initialize available ETW Providers
                    string[] lines = File.ReadAllLines(providerList);
                    string[] providersETW = new string[lines.Length];

                    // Prepare exported providers read from file
                    for (int i = 0; i < lines.Length; i++)
                        providersETW[i] = (lines[i].Substring(0, lines[i].IndexOf(" {") + 1)).TrimEnd();

                    // Enable UserTrace Providers
                    foreach (string provider in providersETW) {
                        if (!provider.Contains("{")) {
                            try {
                                session.EnableProvider(provider, TraceEventLevel.Verbose, 0);
                                Console.WriteLine("Provider: " + provider + " enabled.");
                            }
                            catch {
                                Console.WriteLine("Provider: " + provider + " could not be enabled.");
                            }
                        }
                    }
                }
                else {
                    foreach (var provider in TraceEventProviders.GetPublishedProviders()) {
                        try {
                            session.EnableProvider(provider, TraceEventLevel.Verbose, 0);
                            Console.WriteLine("Provider: " + TraceEventProviders.GetProviderName(provider) + " enabled.");
                        }
                        catch {
                            Console.WriteLine("Provider: " + TraceEventProviders.GetProviderName(provider) + " could not be enabled.");
                        }
                    }
                }
                Console.WriteLine("---------------------------------------------------------------------------------------------------------");

                // Set Trace Event callbacks
                // Kernel Trace - Callback (All Events)
                session.Source.Kernel.All += delegate (TraceEvent data) {
                    if (pidWatchlist.IndexOf(data.ProcessID) != -1) {
                        eventCounter++;
                        // Prepare event data and mark event for CSV export
                        csvFile.AppendLine(PrepareEvent(data));
                        PrintEventCounter();
                    }
                };

                foreach (string n in spawndetection) {
                    var process = Process.GetProcessesByName(n);
                    Console.WriteLine("[SpawnDetection] Already running \"" + n + "\" processes (SpawnDetection only):");
                    foreach (var p in process) {
                        spawnDetectPIDs.Add(p.Id);
                        Console.WriteLine("\t" + p.Id + "\t" + p.ProcessName);
                    }
                    Console.Write("\n");
                }

                // KernelTrace special Callback Process START
                session.Source.Kernel.ProcessStart += delegate (ProcessTraceData data) {
                    // check if process name is in list (used for additional process names and spawned pids)
                    int indexPName = pname.FindIndex(processName => processName.Equals(data.ProcessName, StringComparison.OrdinalIgnoreCase));
                    int indexSpawnDetection = spawndetection.FindIndex(processName => processName.Equals(data.ProcessName, StringComparison.OrdinalIgnoreCase));

                    if (pidWatchlist.IndexOf(data.ParentID) != -1 || spawnDetectPIDs.IndexOf(data.ParentID) != -1) {
                        pidWatchlist.Add(data.ProcessID);

                        eventCounter++;
                        csvFile.AppendLine(PrepareEvent(data));
                        PrintEventCounter();

                        Console.SetCursorPosition(0, Console.CursorTop);
                        Console.WriteLine("[" + data.TimeStamp.ToString("HH:mm:ss") + "] [PROCESS CREATED] " + "PID: " + data.ProcessID + " (PPID: " + data.ParentID + ") ProcessName: \"" + data.ProcessName + "\" ImagePath: \"" + data.ImageFileName + "\" Args: \"" + data.CommandLine + "\"");
                    }
                    else if (indexPName != -1) { // additional process names (ProcessName)
                        pidWatchlist.Add(data.ProcessID);

                        eventCounter++;
                        csvFile.AppendLine(PrepareEvent(data));
                        PrintEventCounter();

                        Console.SetCursorPosition(0, Console.CursorTop);
                        Console.WriteLine("[" + data.TimeStamp.ToString("HH:mm:ss") + "] [ProcessName] [PROCESS CREATED] " + "PID: " + data.ProcessID + " (PPID: " + data.ParentID + ") ProcessName: \"" + data.ProcessName + "\" ImagePath: \"" + data.ImageFileName + "\" Args: \"" + data.CommandLine + "\"");
                    }
                    else if (indexSpawnDetection != -1) { // SpawnDetection
                        spawnDetectPIDs.Add(data.ProcessID);

                        eventCounter++;
                        csvFile.AppendLine(PrepareEvent(data));
                        PrintEventCounter();

                        Console.SetCursorPosition(0, Console.CursorTop);
                        Console.WriteLine("[" + data.TimeStamp.ToString("HH:mm:ss") + "] [SpawnDetection] [PROCESS CREATED (SpawnDetection only)] " + "PID: " + data.ProcessID + " (PPID: " + data.ParentID + ") ProcessName: \"" + data.ProcessName + "\" ImagePath: \"" + data.ImageFileName + "\" Args: \"" + data.CommandLine + "\"");
                    }
                };

                // KernelTrace special Callback Process STOP
                session.Source.Kernel.ProcessStop += delegate (ProcessTraceData data) {
                    if (pidWatchlist.IndexOf(data.ProcessID) != -1 || pidWatchlist.IndexOf(data.ParentID) != -1 || spawnDetectPIDs.IndexOf(data.ProcessID) != -1) {
                        pidWatchlist.Remove(data.ProcessID);
                        if (spawnDetectPIDs.IndexOf(data.ProcessID) != -1) {
                            spawnDetectPIDs.Remove(data.ProcessID);
                            Console.WriteLine("[" + data.TimeStamp.ToString("HH:mm:ss") + "]" + " [SpawnDetection] Child spawn detection stopped for process (PID: " + data.ProcessID + " terminated)");
                        }
                        eventCounter++;
                        csvFile.AppendLine(PrepareEvent(data));
                        PrintEventCounter();

                        Console.SetCursorPosition(0, Console.CursorTop);
                        Console.WriteLine("[" + data.TimeStamp.ToString("HH:mm:ss") + "]" + " [PROCESS TERMINATED] " + "PID: " + data.ProcessID + " (PPID: " + data.ParentID + ") ProcessName: \"" + data.ProcessName + "\" ImagePath: \"" + data.ImageFileName + "\" Args: \"" + data.CommandLine + "\"");
                    }
                };

                // User Trace - Callback (All Events)
                session.Source.Dynamic.All += delegate (TraceEvent data) {
                    if (pidWatchlist.IndexOf(data.ProcessID) != -1) {
                        eventCounter++;
                        // Prepare event data and mark event for CSV export
                        csvFile.AppendLine(PrepareEvent(data));
                        PrintEventCounter();
                    }
                };

                // Callback for unhandled events
                session.Source.UnhandledEvents += delegate (TraceEvent data) {
                    if (pidWatchlist.IndexOf(data.ProcessID) != -1) {
                        eventCounter++;
                        csvFile.AppendLine(PrepareEvent(data));
                        PrintEventCounter();
                    }
                };

                // Timer
                if (timersec != 0) {
                    var timer = new Timer(delegate (object state) {
                        Console.ForegroundColor = ConsoleColor.DarkYellow;
                        Console.WriteLine("\n\n[TIMERINFO] ETWProcessTracer stopped after {0} sec\n[INFO] ETWProcessTracerSession will be terminated. Please wait...", timersec);
                        Console.ResetColor();
                        session.Source.StopProcessing();
                    }, null, timersec * 1000, Timeout.Infinite);

                    session.Source.Process();
                    File.WriteAllText(csvFileName, csvFile.ToString());
                    timer.Dispose();
                }
                else {
                    session.Source.Process();
                    File.WriteAllText(csvFileName, csvFile.ToString());
                }
            }
        }

        public class CommandLineOptions {
            [Option("PID", Required = true, HelpText = "\nInitial root ProcessID to analyze. (Spawned child processes will be \nautomatically added to the watchlist (full analyze).\nExample: --PID 1234")]
            public int ArgProcessID { get; set; }

            [Option("ProcessName", Separator = ',', Required = false, HelpText = "\n(Optional) Additional list of processes to analyze (by name) \nPID(s) will be added automatically to the watchlist (full analyze) \nwhen the process (name) is spawned by any parent process(es) \nExample(s): --ProcessName powershell\n            --ProcessName powershell,wmiprsve,msiexec,...")]
            public IEnumerable<string> ArgPNames { get; set; }

            [Option("SpawnDetection", Separator = ',', Required = false, HelpText = "\n(Optional) Additional list of processes (by name) for spawn detection only (!) \nChild processes PID will be added automatically to the watchlist (full analyze)\nwhen a new process is spawned by the parent process.\nExample(s): --SpawnDetection wmiprsve\n            --SpawnDetection powershell,wmiprsve,msiexec,...\nProcessName has a higher precedence (if same an equal name is specified \nin both options)")]
            public IEnumerable<string> ArgSpawnDetection { get; set; }

            [Option("ProviderConfig", Required = false, HelpText = "(Optional, Default: All available registered ETW-Provders will be enabled.)\nPath to list of (individual selected) ETW-Provider(s) \n(e.g. export of \"logman query providers\")\nFormat: ProviderName\t{GUID}\nExample: --ProviderConfig C:\\etw\\providers.txt")]
            public string ArgProviders { get; set; }

            [Option("KernelKeywords", Separator = ',', Required = false, HelpText = "(Optional, Keyword: \"Process\" is always enabled by default.)\nDiskFileIO,DiskIO,ImageLoad,MemoryHardFaults,NetworkTCPIP,ProcessCounters,\nProfile,Thread,ContextSwitch,DiskIOInit,Dispatcher,FileIO,FileIOInit,Memory,\nRegistry,SystemCall,VirtualAlloc,VAMap,AdvancedLocalProcedureCalls,\nDeferedProcedureCalls,Driver,Interrupt,SplitIO\n\nForensic (Process,Thread,ImageLoad,NetworkTCPIP,FileIOInit,Registry)\nAll (Verbose,ContextSwitch,Dispatcher,FileIO,FileIOInit,Memory,Registry,\n\t\t\t\t VirtualAlloc,VAMap,SystemCall,OS)\nDefault (DiskIO,DiskFileIO,DiskIOInit,ImageLoad,MemoryHardFaults,NetworkTCPIP,\n         Process,ProcessCounters,Profile,Thread) \nVerbose (Default,ContextSwitch,Dispatcher,FileIO,FileIOInit,Memory,Registry,\n         VirtualAlloc,VAMap) \nOS (AdvancedLocalProcedureCalls,DeferedProcedureCalls,Driver,Interrupt,SplitIO) \nThreadTime (Default,ContextSwitch,Dispatcher)\n\nExamples: --KernelKeywords NetworkTCPIP,Registry,FileIOInit\n          --KernelKeywords Forensic")]
            public IEnumerable<string> ArgKernelKeywords { get; set; }

            [Option("dir", Required = false, HelpText = "(Optional) Change path of logfile out directory \n" + @"Example: --dir C:\etw\logs\")]
            public string ArgOutFolder { get; set; }

            [Option("timer", Required = false, Default = 0, HelpText = "\n(Optional) Set Timer (secs.)) \nExample: --timer 30")]
            public int ArgTimer { get; set; }
        }

        private static string PrepareEvent(TraceEvent data) {
            string datadump = data.ToString();
            datadump = datadump.Replace(System.Environment.NewLine, " ");
            datadump = datadump.Replace(";", " ");
            datadump = datadump.Replace("&quot", " ");
            var n = string.Format("{0};{1};{2};{3};{4};{5}", eventCounter.ToString(), data.TimeStamp.ToString("dd/MM/yyy HH:mm:ss"), data.ProcessID.ToString(), data.ProviderName.ToString(), data.EventName, datadump);

            return n;
        }
        private static void PrintEventCounter() {
            Console.SetCursorPosition(0, Console.CursorTop);
            Console.Write("Events: " + eventCounter);
        }
    }
}